try:
    from ConfigParser import SafeConfigParser as ConfigParser
except ImportError:
    from configparser import RawConfigParser as ConfigParser

import os

from fabric.api import *
from fabric.colors import red, green, yellow
from fabric.contrib.console import confirm


DEPLOYMENT = None

PROJECT_PATH = os.path.dirname(env.real_fabfile)
VENV_PATH = PROJECT_PATH + "/devenv"


def prod():
    global DEPLOYMENT
    DEPLOYMENT = 'prod'
    env.user = 'mreinhardt'
    # env.key_filename = 'C:\cygwin64\home\Marcel\.ssh\id_rsa'
    env.hosts = ["vervos.net"]


def compile_translations():
    with lcd(PROJECT_PATH + "/src/dlrg_ravision2"):
        local("../../devenv/bin/python ../manage_local.py compilemessages")


def deploy():
    global DEPLOYMENT
    if DEPLOYMENT is None:
        abort(red("no target specified"))
        DEPLOYMENT

    # GET PREVIOUSLY DEPLOYED REV
    env.warn_only = True
    dir_base = "/var/www/vervos.net/"
    rev_prev = sudo("readlink " + dir_base + "deployed")
    rev_head = local("git rev-parse HEAD", capture=True).strip()

    if rev_prev == rev_head:
        abort(red("Revision " + rev_head + " is already deployed for this target."))

    env.warn_only = True
    print(green("Printing commits to be deployed..."))
    local("git log {0}..{1}".format(rev_prev, rev_head))
    env.warn_only = False

    if not confirm(yellow("Are you sure you want to deploy revision ") + red(rev_head) + yellow(" to ") +
                   red(DEPLOYMENT) + yellow("?"), default=False):
        abort(red("Aborted by user."))

    dir_destination = dir_base + rev_head

    # SET UP VIRTUALENV
    dir_tmp = sudo("mktemp -d")
    with cd(dir_tmp):
        print(green("Creating virtualenv..."))
        sudo("virtualenv -p /usr/bin/python3.4 virtualenv")
        sudo(". virtualenv/bin/activate && pip install --upgrade pip")

    # CREATE ARCHIVE
    print(green("Archiving..."))
    local("git archive -o deploy.tar HEAD")

    print(green("Compressing..."))
    local("gzip deploy.tar")

    # UPLOAD
    dir_archive = dir_tmp + "/archive"
    sudo("mkdir " + dir_archive)
    sudo("chmod o+x " + dir_tmp)
    sudo("chmod o+rwx " + dir_archive)
    put("deploy.tar.gz", dir_archive + "/deploy.tar.gz")
    local("rm deploy.tar.gz")
    with cd(dir_archive):
        sudo("tar xf deploy.tar.gz")

    # INSTALL DEPENDENCIES
    print(green("Installing dependencies..."))
    sudo(". " + dir_tmp + "/virtualenv/bin/activate && pip install -r " + dir_archive + "/requirements-frozen.txt")

    # COPY
    dir_modules = dir_tmp + "/modules"
    sudo("mkdir " + dir_modules)
    print(green("Copying code into temp directory..."))
    sudo("cp -r " + dir_archive + "/src/* " + dir_modules)
    sudo("rm -r " + dir_archive)

    # CREATE TEMPORARY secrets.cfg SO SETTINGS CAN BE IMPORTED
    sudo("echo '[postgres]' > {}/dlrg_revision2/secrets.cfg".format(dir_modules))
    sudo("echo 'psql_dbname = x' >> {}/dlrg_revision2/secrets.cfg".format(dir_modules))
    sudo("echo 'psql_user = y' >> {}/dlrg_revision2/secrets.cfg".format(dir_modules))
    sudo("echo 'psql_pass = y' >> {}/dlrg_revision2/secrets.cfg".format(dir_modules))
    sudo("echo '[mail]' >> {}/dlrg_revision2/secrets.cfg".format(dir_modules))
    sudo("echo 'mail_url = x' >> {}/dlrg_revision2/secrets.cfg".format(dir_modules))
    sudo("echo 'mail_user = y' >> {}/dlrg_revision2/secrets.cfg".format(dir_modules))
    sudo("echo 'mail_port = y' >> {}/dlrg_revision2/secrets.cfg".format(dir_modules))
    sudo("echo 'mail_pass = y' >> {}/dlrg_revision2/secrets.cfg".format(dir_modules))

    # COLLECT STATIC FILES
    sudo("mkdir " + dir_tmp + "/static")
    with cd(dir_modules):
        print(green("Collecting static files from app dirs..."))
        sudo(". {}/virtualenv/bin/activate && python ./manage_{}.py collectstatic --noinput".format(dir_tmp,
                                                                                                    DEPLOYMENT))

    # RELOCATE VIRTUALENV
    print(green("Relocating virtualenv..."))
    with cd(dir_tmp):
        sudo("virtualenv --relocatable virtualenv")

    # XXX This sucks, but "virtualenv --relocatable" simply does not update this.
    sudo("sed -i '/^VIRTUAL_ENV=/ s#.*#VIRTUAL_ENV=\"{}/deployed/virtualenv\"#' {}/virtualenv/bin/activate".format(
        dir_base, dir_tmp))

    # REMOVE TEMPORARY secrets.cfg
    sudo("rm {}/dlrg_revision2/secrets.cfg".format(dir_modules))

    sudo("mkdir " + dir_destination)
    print(green("Moving code into final destination..."))
    sudo("cp -r " + dir_tmp + "/* " + dir_destination)

    print(green("Restoring symlink to secrets.cfg..."))
    sudo("ln -s " + dir_base + "secrets.cfg " + dir_destination + "/modules/dlrg_revision2/secrets.cfg")

    # cleanup
    print(green("Cleaning up temp directory..."))
    sudo("rm -r " + dir_tmp)

    print(green("Activating revision {} for target {}...".format(rev_head, DEPLOYMENT)))

    print(green("Stopping gunicorn..."))
    try:
        sudo("service gunicorn stop")
        pass
    except:
        pass

    print(green("Stopping celery..."))
    try:
        sudo("service celery stop")
        pass
    except:
        pass

    print(green("Changing symbolic link..."))
    with cd(dir_base):
        sudo("ln -sfT " + rev_head + " deployed")

    print(green("Running database migrations..."))
    sudo("{}deployed/modules/manage_{}.py migrate".format(dir_base, DEPLOYMENT))

    print(green("Starting celery..."))
    sudo("service celery start")

    print(green("Starting gunicorn..."))
    sudo("service gunicorn start")

    print(green("DEPLOYMENT COMPLETE, cleaning up"))
    sudo("rm -r " + dir_base + rev_prev)


def devsetup():
    project_path = os.path.dirname(env.real_fabfile)
    venv_path = project_path + '/devenv'
    if os.path.exists(venv_path):
        print(red('virtualenv dir already exists'))
    else:
        with cd(project_path):
            print(green('creating virtualenv...'))
            local('virtualenv -p /usr/bin/python3.4 ' + venv_path)
            print(green('installing dependencies...'))
            local(venv_path + '/bin/pip install -r ' + project_path + '/requirements-frozen.txt')
            print(green('linking vervos into sys.path...'))
            local('ln -s {0} {1}'.format(
                project_path + '/src/dlrg_revision2',
                venv_path + '/lib/python3.4/site-packages',
            ))

    if os.path.exists(project_path + "/src/dlrg_revision2/secrets.cfg"):
        print(yellow("secrets.cfg already exists"))
    else:
        secrets = ConfigParser()
        secrets.add_section("django")
        secrets.set("django", "secret_key", "REPLACE_ME")
        secrets.add_section("postgres")
        secrets.set("postgres", "psql_dbname", "REPLACE_ME")
        secrets.set("postgres", "psql_user", "jdoe")
        secrets.set("postgres", "psql_pass", "REPLACE_ME")
        secrets.add_section("mail")
        secrets.set("mail_url", "mail_url", "REPLACE_ME")
        secrets.set("mail_user", "mail_user", "jdoe")
        secrets.set("mail_port", "mail_port", "143")
        secrets.set("mail_pass", "mail_pass", "REPLACE_ME")

        with open(project_path + "/src/dlrg_revision2/secrets.cfg", 'wb') as f:
            secrets.write(f)
        print(red("src/dlrg_revision2/secrets.cfg has been created with dummy "
                  "values! You should replace them with the current "
                  "test credentials."))

    if os.path.exists(project_path + '/devdb.sqlite'):
        print(red('dev db already exists'))
    else:
        with cd(project_path):
            print(green('creating initial database and running initial schema migrations...'))
            local('./manage migrate')


def windowsdevsetup():
    project_path = os.path.dirname(env.real_fabfile)
    venv_path = project_path + '/devenv'
    if os.path.exists(venv_path):
        print(red('virtualenv dir already exists'))
    else:
        with cd(project_path):
            print('creating virtualenv...')
            local('virtualenv -p c:\Python34\python.exe ' + venv_path)
            print('installing dependencies...')
            local(venv_path + '/Scripts/pip install -r ' + project_path + '/requirements-frozen-windows.txt')
            local(venv_path + '/Scripts/pip install fabric')
            print('linking dlrg_revision2 into sys.path...')
            local('cmd /c mklink /D \"{0}\" \"{1}\"'.format(
                venv_path + '\Lib\site-packages\dlrg_revision2',
                project_path + '\src\dlrg_revision2',
            ))

    if os.path.exists(project_path + "/src/dlrg_revision2/secrets.cfg"):
        print(yellow("secrets.cfg already exists"))
    else:
        secrets = ConfigParser()
        secrets.add_section("django")
        secrets.set("django", "secret_key", "REPLACE_ME")
        secrets.add_section("postgres")
        secrets.set("postgres", "psql_dbname", "REPLACE_ME")
        secrets.set("postgres", "psql_user", "jdoe")
        secrets.set("postgres", "psql_pass", "REPLACE_ME")
        secrets.add_section("mail")
        secrets.set("mail_url", "mail_url", "REPLACE_ME")
        secrets.set("mail_user", "mail_user", "jdoe")
        secrets.set("mail_port", "mail_port", "143")
        secrets.set("mail_pass", "mail_pass", "REPLACE_ME")

        with open(project_path + "/src/dlrg_revision2/secrets.cfg", 'wb') as f:
            secrets.write(f)
        print(red("src/dlrg_revision2/secrets.cfg has been created with dummy "
                  "values! You should replace them with the current "
                  "test credentials."))

    if os.path.exists(project_path + '/devdb.sqlite'):
        print(red('dev db already exists'))
    else:
        with cd(project_path):
            print(green('creating initial database and running initial schema migrations...'))
            local(venv_path + '/Scripts/python ' + 'src/manage_local.py migrate')
