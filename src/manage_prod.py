#!/var/www/vervos.net/deployed/virtualenv/bin/python
import sys

import os


if __name__ == "__main__":
    if "virtualenv" not in sys.executable:
        print("Please use the Python binary located in the virtualenv.")
        print("Just running this script as an executable should to the trick.")
        sys.exit(1)

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dlrg_revision2.settings.prod")
    sys.path.append("/var/www/vervos.net/deployed/modules")
    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)
