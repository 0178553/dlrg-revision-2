#!/var/www/vervos.net/deployed/virtualenv/bin/python

import sys
import os
import site

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dlrg_revision2.settings.prod")
sys.path.insert(1, os.path.dirname(os.path.realpath(__file__)))

site.addsitedir('/var/www/vervos.net/deployed/virtualenv/lib/python3.4/site-packages')
sys.path.append("/var/www/vervos.net/deployed/modules")


from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
