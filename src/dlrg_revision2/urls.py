from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
                       url(r'^$', 'dlrg_revision2.apps.webapp.views.home', name='home'),
                       url(r'^admin/', include(admin.site.urls)),
                       )
