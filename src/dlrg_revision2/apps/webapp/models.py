# coding: utf8

from django.db import models
from django.utils import timezone
from django.utils.encoding import smart_text


NAME_CHOICES = (
    (1, 'Anfängergruppe'),
    (2, 'Voranmeldung'),
    (3, 'Seepferdchen'),
    (4, 'Bronze'),
    (5, 'Silber'),
    (6, 'Gold'),
)


class Person(models.Model):
    first_name = models.CharField(
        max_length=255,
        verbose_name='Vorname'
    )
    last_name = models.CharField(
        max_length=255,
        verbose_name='Nachname'
    )
    birthday = models.DateField(
        verbose_name='Geburtstag',
        null=True,
        blank=True,
        help_text='Das Datum muss in folgendem Format angegeben werden: JAHR-MONAT-TAG. Zum Beispiel 2001-04-01.',
    )
    city = models.CharField(
        null=True,
        max_length=254,
        blank=True,
        verbose_name='Ort',
    )
    created = models.DateTimeField(
        default=timezone.now,
        verbose_name='Erstellt',
        blank=True,
    )
    email = models.EmailField(
        null=True,
        max_length=254,
        blank=True,
        verbose_name='E-Mail Adresse',
    )
    phone = models.CharField(
        blank=True,
        max_length=24,
        null=True,
        verbose_name='Telefon',
    )
    phone2 = models.CharField(
        blank=True,
        max_length=24,
        null=True,
        verbose_name='2. Telefon',
    )
    street = models.CharField(
        null=True,
        max_length=254,
        blank=True,
        verbose_name='Straße',
    )
    updated = models.DateTimeField(
        auto_now=True,
        blank=True,
        verbose_name='Zuletzt geändert'
    )
    zip_code = models.CharField(
        max_length=5,
        blank=True,
        null=True,
        verbose_name='PLZ',
    )


class Ausbilder(Person):
    class Meta:
        verbose_name = 'Ausbilder'
        verbose_name_plural = 'Ausbilder'

    qualification = models.ManyToManyField('AusbilderQualifikation', blank=True, verbose_name="Qualifikation")

    def __str__(self):
        return smart_text(self.first_name + " " + self.last_name)


class Schueler(Person):
    class Meta:
        verbose_name = 'Schüler'
        verbose_name_plural = 'Schüler'

    group = models.ForeignKey('Group', null=True, blank=True, verbose_name='Gruppe')
    qualification = models.ForeignKey('SchuelerQualifikation', null=True, blank=True, verbose_name="Qualifikation")
    description = models.TextField(null=True, blank=True, verbose_name="Beschreibung")

    def __str__(self):
        return smart_text(self.first_name + " " + self.last_name)


class Qualifikation(models.Model):
    description = models.TextField(null=True, blank=True, verbose_name="Beschreibung")


class AusbilderQualifikation(Qualifikation):
    class Meta:
        verbose_name = 'Qualifikationen der Ausbilder'
        verbose_name_plural = 'Qualifikationen der Ausbilder'

    name = models.CharField(max_length=255, null=True)

    def __str__(self):
        return smart_text(self.name)


class SchuelerQualifikation(Qualifikation):
    class Meta:
        verbose_name = 'Qualifikationen der Schüler'
        verbose_name_plural = 'Qualifikationen der Schüler'

    name = models.CharField(max_length=255)

    def __str__(self):
        return smart_text(self.name)


class Group(models.Model):
    class Meta:
        verbose_name = 'Gruppe'
        verbose_name_plural = 'Gruppen'

    max_people = models.IntegerField(
        verbose_name='Max. Teilnehmer',
    )
    min_quali = models.IntegerField(
        choices=NAME_CHOICES,
        verbose_name='Mindestqualifikation',
    )
    name = models.CharField(
        max_length=20,
    )
    ausbilder = models.ManyToManyField(Ausbilder)

    def __str__(self):
        return smart_text(self.name)
