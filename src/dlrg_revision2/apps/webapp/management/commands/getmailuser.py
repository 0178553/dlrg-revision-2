from django.core.management.base import BaseCommand

from dlrg_revision2.apps.webapp import parse_mail


class Command(BaseCommand):
    help = 'Starts parse_mail script'

    def handle(self, *args, **options):
        parse_mail.start()
        self.stdout.write('Finished...')
