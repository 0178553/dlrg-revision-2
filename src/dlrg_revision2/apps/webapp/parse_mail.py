import os
import uuid
import imaplib
import quopri

from django.conf import settings
from dateutil.parser import parse

from dlrg_revision2.apps.webapp.models import Schueler, SchuelerQualifikation


def splitstring(x):
    return x.rstrip('\n').split(":  ")


def get_schueler_dict(filename):
    schueler_mapping = {
        'VORNAME': 'first_name',
        'NACHNAME': 'last_name',
        'GEBURTSDATUM': 'birthday',
        'STRASSE': 'street',
        'POSTLEITZAHL': 'zip_code',
        'ORT': 'city',
        'TELEFON': 'phone',
        'EMAIL': 'email',
        'BEMERKUNG': 'description',
    }
    schueler_dict = {}
    with open(filename) as f:
        for line in f:
            splitted_string = splitstring(line)
            if splitted_string[0] in schueler_mapping:
                if splitted_string[0] == 'GEBURTSDATUM':
                    splitted_string[1] = parse(splitted_string[1]).strftime('%Y-%m-%d')
                schueler_dict[schueler_mapping[splitted_string[0]]] = splitted_string[1]
    return schueler_dict


def get_or_create_schueler(filename):
    quali_object, created = SchuelerQualifikation.objects.get_or_create(name='Voranmeldung')
    schueler_dict = get_schueler_dict(filename)
    first_name = schueler_dict['first_name']
    del schueler_dict['first_name']
    last_name = schueler_dict['last_name']
    del schueler_dict['last_name']
    birthday = schueler_dict['birthday']
    del schueler_dict['birthday']
    schueler, created = Schueler.objects.get_or_create(
        first_name=first_name,
        last_name=last_name,
        birthday=birthday,
        qualification=quali_object,
        defaults=schueler_dict,
        )

    schueler.save()
    return schueler


def get_mail():
    mail_url = settings.MAIL_URL
    mail_port = settings.MAIL_PORT
    mail_user = settings.MAIL_USER
    mail_pass = settings.MAIL_PASS

    mail = imaplib.IMAP4(mail_url, mail_port)
    mail.login(mail_user, mail_pass)  # user / password
    mail.select("INBOX")

    result, data = mail.uid('search', None, "ALL")  # search and return uids instead
    i = len(data[0].split())

    for x in range(i):
        latest_email_uid = data[0].split()[x]
        result, email_data = mail.uid('fetch', latest_email_uid, '(RFC822)')
        raw_email = email_data[0][1]
        msg = quopri.decodestring(raw_email).decode("UTF-8")
        filename = uuid.uuid4().hex
        with open('mailbox/{}'.format(filename), 'w') as f:
            f.write(msg)
        result = mail.uid('COPY', latest_email_uid, 'INBOX/Archiv')
        if result[0] == 'OK':
            mail.uid('STORE', latest_email_uid, '+FLAGS', '(\Deleted)')
            mail.expunge()
    mail.close()
    mail.logout()
    return True


def start():
    if not os.path.exists("mailbox"):
        os.makedirs("mailbox")
    get_mail()
    for file in os.listdir("mailbox/"):
        get_or_create_schueler("mailbox/{}".format(file))
        os.remove("mailbox/{}".format(file))