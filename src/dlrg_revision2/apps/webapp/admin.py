from django.contrib import admin
from easy_select2 import select2_modelform

from dlrg_revision2.apps.webapp.models import Group, Ausbilder, Schueler, SchuelerQualifikation, AusbilderQualifikation


AusbilderQualiForm = select2_modelform(AusbilderQualifikation, attrs={'width': '300px'})
SchuelerQualiForm = select2_modelform(SchuelerQualifikation, attrs={'width': '300px'})
SchuelerForm = select2_modelform(Schueler, attrs={'width': '300px'})
GroupForm = select2_modelform(Group, attrs={'width': '300px'})
AusbilderForm = select2_modelform(Ausbilder, attrs={'width': '300px'})


class GroupAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'min_quali', 'max_people', 'ausbilder',)
        }),
    )
    list_display = ('name', 'min_quali', 'max_people')
    list_editable = ('min_quali', 'max_people')
    ordering = ('name',)
    form = GroupForm


class AusbilderAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('first_name', 'last_name', 'email', 'qualification', 'birthday', 'phone', 'phone2',
                       'created', 'updated')
        }),
        ('Adresse', {
            'fields': ('city', 'zip_code', 'street')
        }),
    )
    list_display = ('first_name', 'last_name', 'email')
    list_editable = ('email',)
    ordering = ('last_name',)
    readonly_fields = ('created', 'updated')
    form = AusbilderForm


class SchuelerAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('first_name', 'last_name', 'email', 'qualification', 'birthday', 'phone', 'phone2', 'group',
                       'created', 'updated')
        }),
        ('Adresse', {
            'fields': ('city', 'zip_code', 'street')
        }),
    )
    form = SchuelerForm
    list_display = ('first_name', 'last_name', 'email',)
    list_editable = ('email',)
    ordering = ('last_name',)
    readonly_fields = ('created', 'updated')


class AusbilderQualifikationAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'description')
        }),
    )
    list_display = ('name',)
    form = AusbilderQualiForm


class SchuelerQualifikationAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'description')
        }),
    )
    list_display = ('name',)
    form = SchuelerQualiForm


admin.site.register(Ausbilder, AusbilderAdmin)
admin.site.register(Schueler, SchuelerAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(AusbilderQualifikation, AusbilderQualifikationAdmin)
admin.site.register(SchuelerQualifikation, SchuelerQualifikationAdmin)