try:
    from ConfigParser import NoSectionError, SafeConfigParser as ConfigParser
except ImportError:
    from configparser import NoSectionError, ConfigParser

from os.path import dirname, join, realpath

PROJECT_ROOT = realpath(dirname(dirname(__file__)))

secrets = ConfigParser()
secrets.read(join(PROJECT_ROOT, "secrets.cfg"))

ADMINS = (
    ("Marcel Reinhardt", "dakusaido996@gmail.com"),
)

APP_NAME = 'DLRG-Web'
APP_VERSION = "0.5"

DJANGO_INSTALLED_APPS = [
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

EXTERNAL_INSTALLED_APPS = [
    'bootstrap3',
    'easy_select2',
    'kombu.transport.django',
]

INTERNAL_INSTALLED_APPS = [
    'dlrg_revision2.apps.webapp',
]

INSTALLED_APPS = DJANGO_INSTALLED_APPS + EXTERNAL_INSTALLED_APPS + INTERNAL_INSTALLED_APPS

LANGUAGE_CODE = "de-de"

MANAGERS = ADMINS

MAIL_URL = secrets.get("mail", "mail_url")
MAIL_PORT = secrets.get("mail", "mail_port")
MAIL_USER = secrets.get("mail", "mail_user")
MAIL_PASS = secrets.get("mail", "mail_pass")

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = "dlrg_revision2.urls"

try:
    SECRET_KEY = secrets.get("django", "secret_key")
except NoSectionError:
    # this happens during deployment in /tmp
    SECRET_KEY = "somehow_required_by_collectstatic"


# remember this is hardcoded in the error page templates (e.g. 500.html)
STATIC_URL = "/static/"

STATICFILES_DIRS = (
    join(PROJECT_ROOT, "static"),
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_DIRS = (
    join(PROJECT_ROOT, "templates"),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.csrf',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
)

TIME_ZONE = "Europe/Berlin"

USE_I18N = True
USE_L10N = True
USE_THOUSAND_SEPARATOR = False