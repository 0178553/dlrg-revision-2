from .base import *

ALLOWED_HOSTS = [
    "vervos.net",
    "localhost",
    "127.0.0.1",
]

BASE_URL = "http://127.0.0.1:8080"

DEPLOYMENT = 'prod'

DEBUG = False
TEMPLATE_DEBUG = DEBUG

STATIC_ROOT = realpath(dirname(dirname(PROJECT_ROOT))) + "/static"

INTERNAL_IPS = ("127.0.0.1",)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': secrets.get('postgres', 'psql_dbname'),
        'USER': secrets.get('postgres', 'psql_user'),
        'PASSWORD': secrets.get('postgres', 'psql_pass'),
        'HOST': 'localhost',
        'PORT': '',
    }
}