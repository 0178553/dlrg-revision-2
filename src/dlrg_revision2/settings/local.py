from .base import *

ALLOWED_HOSTS = [
    "127.0.0.1",
    'localhost',
]

BASE_URL = "http://127.0.0.1:8000"

DEPLOYMENT = 'local'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

INTERNAL_IPS = ('127.0.0.1',)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': realpath(dirname(dirname(PROJECT_ROOT))) + '/devdb.sqlite'
    }
}