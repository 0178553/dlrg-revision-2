#!/bin/bash
set -e
LOGFILE=/var/www/vervos.net/logs/gunicorn.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=3
USER=www-data
GROUP=www-data
cd /var/www/vervos.net/deployed/modules
. ../virtualenv/bin/activate
gunicorn \
    dlrg_revision2.wsgi \
    -b 127.0.0.1:8080 \
    -w $NUM_WORKERS \
    --user=$USER \
    --group=$GROUP \
    --log-level=debug
    --log-file=$LOGFILE \
    2>>$LOGFILE